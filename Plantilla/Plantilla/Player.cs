﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;

namespace Plantilla
{
    public class Player : Sprite
    {

        public float velocidad;

        public override void Update()
        {
            //Leemos las entradas
            KeyboardState teclado = Keyboard.GetState();

            float x =Posicion.X,  y = Posicion.Y;

            if (teclado.IsKeyDown(Keys.Left) && x -velocidad >= 0)
            {
                x -= velocidad; 
            }
            else if (teclado.IsKeyDown(Keys.Right) && x+velocidad < 924)
            {
                x += velocidad;
            }

            if (teclado.IsKeyDown(Keys.Up) && y - velocidad >0)
            {
                y -= velocidad;
            }
            else if (teclado.IsKeyDown(Keys.Down) && y +velocidad < 924)
            {
                y += velocidad;
            }

            Posicion = new Vector2(x, y);

        }
    }
}