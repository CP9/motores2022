﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Plantilla
{
    public class Enemigo : Sprite
    {
        public float velocidad = 5;

        public bool paraDestruir = false;

        public override void Update()
        {
            this.Posicion = new Vector2(Posicion.X , this.Posicion.Y + velocidad);

            paraDestruir = (Posicion.Y > 1024);

        }

        public static Enemigo CrearEnemigo()
        {
            Enemigo enemigo = new Enemigo();
            enemigo.Posicion = new Vector2( Helper.rnd.Next(0,924)  , -100);
            enemigo.velocidad = 3;
            enemigo.Tamaño = new Vector2(100, 100);
            return enemigo;

        }

    }
}