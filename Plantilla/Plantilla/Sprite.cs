﻿using Cocos2D;
using CocosDenshion;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

using System;

namespace Plantilla
{
    public abstract class Sprite
    {
        //ubicación
        public Vector2 Posicion;
        public Vector2 Tamaño;

        //Textura
        public Texture2D Textura;

        public void CargarTextura(string nombre, ContentManager manejador)
        {
            Textura = manejador.Load<Texture2D>(nombre);
        }


        public abstract void Update();


        public void Dibujar(SpriteBatch sb)
        {
            Rectangle rect = new Rectangle((int)Posicion.X, (int)Posicion.Y, (int)Tamaño.X, (int)Tamaño.Y);
            sb.Draw(Textura, rect, Color.White);
        }



    }
}