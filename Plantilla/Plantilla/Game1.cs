﻿using Cocos2D;
using CocosDenshion;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System;


namespace Plantilla
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        List<Fondo> fondos = new List<Fondo>();

        List<Enemigo> enemigos = new List<Enemigo>();

        DateTime ultimoEnemigo = new DateTime();

        Player jugador;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.PreferredBackBufferWidth = 1024;
            graphics.PreferredBackBufferHeight = 1024;
            //
            // This is the main Cocos2D connection. The CCApplication is the manager of the
            // nodes that define your game.
            //
    
     
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
                
         


        }

 
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            Fondo fondo = Fondo.CrearFondo(0);
            fondo.CargarTextura("agua", Content);
            fondos.Add(fondo);

            fondo = Fondo.CrearFondo(-1023);
            fondo.CargarTextura("agua", Content);
            fondos.Add(fondo);

            jugador = new Player();
            jugador.CargarTextura("nave", Content);

            float mitad = 512 - 50;

            jugador.Posicion = new Vector2(mitad, 800);
            jugador.Tamaño = new Vector2(100, 100);
            jugador.velocidad = 3;

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            TimeSpan diferencia = (DateTime.Now - ultimoEnemigo);

            if ( diferencia.TotalSeconds >2  )
            {
                ultimoEnemigo = DateTime.Now;

                Enemigo enemigo = Enemigo.CrearEnemigo();
                enemigo.CargarTextura("ladrillo", this.Content);
                enemigos.Add(enemigo);

            }
                // For Mobile devices, this logic will close the Game when the Back button is pressed
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                ExitGame();
            }

            foreach (Fondo fondo in fondos)
            {
                fondo.Update();
            }

            foreach (Enemigo malo in enemigos)
            {
                malo.Update();
            }
            jugador.Update();

            for (int indice = enemigos.Count - 1; indice >= 0; indice--)
            {
                if (enemigos[indice].paraDestruir)
                {
                    enemigos.Remove(enemigos[indice]);
                }
            }
            // TODO: Add your update logic here

            base.Update(gameTime);

            GC.Collect();
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();

            foreach(Fondo fondo in fondos)
            { 
                fondo.Dibujar(spriteBatch);
            }

            foreach (Enemigo malo in enemigos)
            {
                malo.Dibujar(spriteBatch);
            }
            jugador.Dibujar(spriteBatch);

            spriteBatch.End();
            base.Draw(gameTime);
        }

        private void ExitGame()
        {
            // TODO: add your exit code here to restore the device to its per-game environment.
            CCSimpleAudioEngine.SharedEngine.RestoreMediaState();
            Exit();
        }

 
    }
}
