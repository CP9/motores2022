﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;

namespace Plantilla
{
    public class Fondo:Sprite
    {
        public float velocidad = 10;
        public override void Update()
        {

            if (Posicion.Y + velocidad < 1024)
            {
                this.Posicion = new Vector2(0, this.Posicion.Y + velocidad);
            }
            else
            {
                this.Posicion = new Vector2(0, -1023);
            }

        }


        public static Fondo CrearFondo(float y)
        {
            Fondo fondo = new Fondo();
            fondo.velocidad = 3;
            fondo.Posicion = new Vector2(0, y);
            fondo.Tamaño = new Vector2(1024, 1024);
            return fondo;
        }

    }
}