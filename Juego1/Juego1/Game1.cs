﻿using Cocos2D;
using CocosDenshion;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Juego1
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Color color;

        Vector2 posicion = new Vector2();
        Texture2D textura;

        SpriteFont fuente;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            this.Window.Title = "Motores Gráficos";
            //
            // This is the main Cocos2D connection. The CCApplication is the manager of the
            // nodes that define your game.
            //
          //  CCApplication application = new AppDelegate(this, graphics);
           // this.Components.Add(application);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
           
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);


            textura = Content.Load<Texture2D>("sonic");

            fuente = Content.Load<SpriteFont>("fonts\\MarkerFelt-16");

            posicion = new Vector2(50, 50);
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // For Mobile devices, this logic will close the Game when the Back button is pressed
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                ExitGame();
            }

            if ((System.DateTime.Now - ultimoCambio).TotalSeconds >= 1)
            {
                ultimoCambio = System.DateTime.Now;
                color = new Color(rnd.Next(0, 255), rnd.Next(0, 255), rnd.Next(0, 255));

            }


            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                posicion = new Vector2(posicion.X - 5, posicion.Y);
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                posicion = new Vector2(posicion.X + 5, posicion.Y);
            }


            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        public static System.Random rnd = new System.Random();

        private System.DateTime ultimoCambio = new System.DateTime();


        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {

         
            GraphicsDevice.Clear(color);


            spriteBatch.Begin();

            Rectangle rect = new Rectangle((int) posicion.X, (int)posicion.Y, 70, 70);

            spriteBatch.Draw(textura, rect, Color.White);

         

            spriteBatch.DrawString(fuente,"Hola clase", new Vector2(5,5),Color.White);


            spriteBatch.End();


            //if ((System.DateTime.Now.Second % 2) == 0)
            //{
            //    GraphicsDevice.Clear(Color.Coral);
            //}
            //else
            //{
            //    GraphicsDevice.Clear(Color.Blue);
            //}




            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }

        private void ExitGame()
        {
            // TODO: add your exit code here to restore the device to its per-game environment.
            CCSimpleAudioEngine.SharedEngine.RestoreMediaState();
            Exit();
        }
    }
}
